import kivy
kivy.require("1.7.0") 
from kivy import Config
Config.set('graphics', 'multisamples', '0')
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import StringProperty
from kivy.uix.label import Label
import pickle
import random



#quiz logic
class Player:
	def __init__(self, name, score):
		self.name=name
		self.score=score
	
	
	
	

def ReadPlayers():
	players = []
	with open("PlayerScores.pickle", "rb") as pickleIn:
		while True:
			try:
				players=(pickle.load(pickleIn))
			except EOFError:
				break
	pickleIn.close()
	return players

def SavePlayers(players):
		with open ("PlayerScores.pickle","wb") as pickleOut:
			pickle.dump(players, pickleOut)
			pickleOut.close()




class Question:
	def __init__(self, question, answer):
		self.question=question
		self.answer=answer

def AddQuestion(questionPool, question, answer):
	
	questionPool.append(Question(question,answer))
	

def ReadQuestions():
	readQuestions = []
	with open("QuestionPool.pickle", "rb") as pickleIn:
		while True:
			try:
				readQuestions=(pickle.load(pickleIn))
			except EOFError:
				break
	pickleIn.close()
	return readQuestions


	

def SaveQuestions(questionsToBeSaved):
	with open ("QuestionPool.pickle","wb") as pickleOut:
		pickle.dump(questionsToBeSaved, pickleOut)
		pickleOut.close()


def GiveQuestion():
	readQuestionPool = []
	readQuestionPool = ReadQuestions()
	oneRandomQuestion = random.choice(readQuestionPool)
	return oneRandomQuestion






#screens logic

class ScreenManagment(ScreenManager):
	def output_a_question(self):
		question=GiveQuestion()
		self.ids.question_goes_here.text=question.question
		self.ids.answer_goes_here.text=question.answer

	def set_player_answer_to_a(self):
		self.ids.player_answer_goes_here.text="a"
	def set_player_answer_to_b(self):
		self.ids.player_answer_goes_here.text="b"
	def set_player_answer_to_c(self):
		self.ids.player_answer_goes_here.text="c"
	
	def check_answer(self):
		if (self.ids.answer_goes_here.text==self.ids.player_answer_goes_here.text):
			return True
		else:
			return False
	
	def player_increase_score(self):
		score=int(self.ids.player_score.text)
		score+=1
		self.ids.player_score.text=str(score)

	def player_save_score(self):
		players=[]
		players=ReadPlayers()
		players.append(Player(self.ids.player_name.text,int(self.ids.player_score.text)))
		SavePlayers(players)
	
	
	def do_nothing(self):
		pass

	def input_a_question(self):
		questionPool = []
		questionPool=ReadQuestions()
		AddQuestion(questionPool,self.ids.player_input_question.text,self.ids.player_input_answer.text)
		SaveQuestions(questionPool)

	def output_high_scores(self):
		players=[]
		players=ReadPlayers()
		sortedplayers= sorted(players, key=lambda x: x.score, reverse=True)
		for player in sortedplayers:
			self.ids.high_score.add_widget(Label(text=player.name+" "+str(player.score)))

	



class WelcomeScreen(Screen):
    pass


class ChooseModeScreen(Screen):
	pass
	


class QuestionAnsweringScreen(Screen):
   	pass
	   


class QuestionAddingScreen(Screen):
    pass
class HighScoreScreen(Screen):
    pass
class PlaceWhereIStoreValues(Screen):
	pass



ui = Builder.load_file('quizui.kv')

class NestoApp(App):
    def build(self):
        return ui

if __name__=="__main__":
    NestoApp().run()
