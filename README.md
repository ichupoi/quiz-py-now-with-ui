# Quiz PY now with UI

Like the previous assignment this is a simple quiz project that reads questions from a pickle file, saves questions to a pickle and keeps player scores 
in a pickle. 
Upon starting the game the players enters his/her respectful name which will be used to keep the player's score until the players closes the game.
The game is played by entering Play Quiz module where random questions questions are given to a player who then gives answers by clicking
a button. Upon giving the right answer to the question the game will supply another random question and increase the player's score by +1. 
When a players submits a  wrong answer the game will return to the main menu and the player's score will be saved.
Every players is able to add his/her questions to the quiz, Label:
text: "These are the high scores", but bear in mind, because of simplicity and my laziness sake, when adding a question
in the top text box enter your question with supplied answer e.g. ("Mary had a little: a)werewolf b)lamb c)kraken"). 
In the bottom text box supply the answer in the from ("a, b or c without any special characters in respect to the correct supplied answer").
By accessing the high score module the game will supply the best player results sorted by their respective score. 